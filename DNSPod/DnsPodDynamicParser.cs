﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;

namespace DNSPod
{
    /// <summary>
    /// JSON转换
    /// </summary>
    public class JsonDynamicParser
    {
        /// <summary>  
        /// 将json字符串转换为dynamic类型
        /// </summary>  
        /// <param name="jsonStr"></param>  
        /// <returns></returns>  
        public static dynamic FromJson(string jsonStr)
        {
            var jss = new JavaScriptSerializer();
            jss.RegisterConverters(new JavaScriptConverter[] { new DynamicJsonConverter() });

            try
            {
                dynamic glossaryEntry = jss.Deserialize(jsonStr, typeof(object)) as DynamicJsonObject;
                return glossaryEntry;
            }
            catch (ArgumentException)
            {
                /*
                 * 经测试，DNSPod服务器接口无法正常调用时，会返回html标签的内容或其他无效json字符串
                 * 将不能正常反序列化，在此捕获异常，默认返回为null，在上一层对dynamic对象进行判断处理
                */
                return null;
            }
        }
    }
    public class DynamicJsonConverter : JavaScriptConverter
    {
        public override object Deserialize(IDictionary<string, object> dictionary, Type type, JavaScriptSerializer serializer)
        {
            if (dictionary == null)
                throw new ArgumentNullException("dictionary");

            return type == typeof(object) ? new DynamicJsonObject(dictionary) : null;
        }

        public override IDictionary<string, object> Serialize(object obj, JavaScriptSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<Type> SupportedTypes
        {
            get { return new ReadOnlyCollection<Type>(new List<Type>(new Type[] { typeof(object) })); }
        }
    }

    public class DynamicJsonObject : DynamicObject
    {
        private IDictionary<string, object> Dictionary { get; set; }

        public DynamicJsonObject(IDictionary<string, object> dictionary)
        {
            this.Dictionary = dictionary;
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = this.Dictionary[binder.Name];

            if (result is IDictionary<string, object>)
            {
                result = new DynamicJsonObject(result as IDictionary<string, object>);
            }
            else if (result is ArrayList)
            {
                result = new List<DynamicJsonObject>((result as ArrayList).ToArray().Select(x => new DynamicJsonObject(x as IDictionary<string, object>)));
            }
            //else if (result is ArrayList && (result as ArrayList) is IDictionary<string, object>)
            //{
            //    result = new List<DynamicJsonObject>((result as ArrayList).ToArray().Select(x => new DynamicJsonObject(x as IDictionary<string, object>)));
            //}
            //else if (result is ArrayList)
            //{
            //    result = new List<object>((result as ArrayList).ToArray());
            //}
            return this.Dictionary.ContainsKey(binder.Name);
        }
    }
}
