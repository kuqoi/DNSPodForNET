﻿
namespace DNSPod
{
    interface IDnsPod
    {
        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="paramObject"></param>
        /// <returns></returns>
        int Create(dynamic paramObject);
        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="paramObject"></param>
        /// <returns></returns>
        dynamic List(dynamic paramObject);

        /// <summary>
        /// 移除
        /// </summary>
        /// <param name="paramObject"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        bool Remove(dynamic paramObject);

        dynamic Info(dynamic paramObject);
    }
}
