﻿using System;

namespace DNSPod
{
    public class DnsPodDomain : DnsPodBase, IDnsPod
    {
        public int Create(dynamic paramObject)
        {
            throw new NotImplementedException();
        }

        public dynamic List(dynamic paramObject)
        {
            return PostApiRequest("Domain.List", paramObject);
        }

        /// <summary>
        /// 根据指定域名名称获取域名ID,若不存在,则返回0
        /// </summary>
        /// <param name="domainName"></param>
        /// <returns></returns>
        public int GetDomainIdByKeywords(string domainName)
        {
            dynamic result = this.List(new { keyword = domainName });
            return Convert.ToInt32(result.status.code) == 1 ? Convert.ToInt32(result.domains[0].id) : 0;
        }

        public bool Remove(dynamic paramObject)
        {
            throw new NotImplementedException();
        }

        public dynamic Info(dynamic paramObject)
        {
            throw new NotImplementedException();
        }
    }
}
