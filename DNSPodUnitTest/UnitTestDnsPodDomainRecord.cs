﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DNSPod;

namespace DNSPodUnitTest
{
    [TestClass]
    public class UnitTestDnsPodDomainRecord
    {
        static int _domainId = 0, _recordId = 0;
        //默认不对主机记录和IP地址进行更改，测试API能否通过
        string subDomain = "ttt", ip = "192.168.199.10";
        [TestMethod]
        public void GetDomainId()
        {
            var domain = new DnsPodDomain();
            dynamic domains = domain.List(new { keyword = "" });//此处填写您域名的名称,为确保准确定位,请填写完整的域名,如:baidu.com
            _domainId = Convert.ToInt32(domains.domains[0].id);
            Assert.AreNotEqual(0, _domainId);
        }
        [TestMethod]
        public void CreateRecord()
        {

            object p = new
            {
                domain_id = _domainId,
                sub_domain = subDomain,
                record_type = "A",
                record_line = "默认",
                value = ip
            };
            var record = new DnsPodRecord();
            _recordId = record.Create(p);
            Assert.AreEqual(true, _recordId > 0);
        }
        [TestMethod]
        public void ModifyRecord()
        {
            object p = new
            {
                domain_id = _domainId,
                record_id = _recordId,
                sub_domain = subDomain,
                record_type = "A",
                record_line = "默认",
                value = ip
            };
            var record = new DnsPodRecord();
            var result = record.Modify(p);
            Assert.AreEqual(true, result);
        }
        [TestMethod]
        public void Ddns()
        {
            var record = new DnsPodRecord();
            var result = record.Ddns(_domainId, _recordId, subDomain, ip);
            Assert.AreEqual(true, result);
        }
        [TestMethod]
        public void RemoveRecord()
        {
            var record = new DnsPodRecord();
            var result = record.Remove(_domainId, _recordId);
            Assert.AreEqual(true, result);
        }

    }
}
