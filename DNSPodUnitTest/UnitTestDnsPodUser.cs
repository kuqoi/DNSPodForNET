﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DNSPod;

namespace DNSPodUnitTest
{
    [TestClass]
    public class UnitTestDnsPodUser
    {

        [Ignore]
        public void GetVersion()
        {
            var result = new DnsPodUser().Version();
            Assert.AreEqual("4.6", result.status.message);
        }

        [Ignore]
        public void UserInfo()
        {
            var user = new DnsPodUser().UserDetail();
            Assert.AreEqual("", user.info.user.real_name);
        }
        [Ignore]
        public void ModifyUserPassword()
        {
            var flag = new DnsPodUser().ModifyUserPassword("your old password here","new password");
            Assert.AreEqual(true,flag);
        }
    }
}
